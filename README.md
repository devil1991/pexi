Pexi
=====

[![npm](https://img.shields.io/npm/v/pexi.svg)](https://www.npmjs.com/package/pexi)
[![pipeline status](https://gitlab.com/griest/pexi/badges/master/pipeline.svg)](https://gitlab.com/griest/pexi/commits/master)
[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)
[![npm](https://img.shields.io/npm/l/pexi.svg)](https://gitlab.com/griest/pexi/LICENSE)

A set of utilities for Pixi.js based on [Hexi](https://github.com/kittykatattack/hexi) by [kittykatattack](https://github.com/kittykatattack).
Each package is released as it own npm package but they are all combined here for easy installation.

Each package matches the major and minor version of the version of Pixi.js that it targets.

Packages:
- [Charm](src/charm): an easy to use tweening library for the Pixi 2D rendering
engine.
- [Dust](src/dust): a quick and easy particle effects engine for Pixi
- [Game Utilities](src/game-utilities): a library of helpful functions for making games and applications for the Pixi rendering engine
- [Smoothie](src/smoothie): a super-fast and lightweight utility that gives you ultra-smooth sprite animation for the Pixi renderer using true delta-time interpolation
- [Sprite Utilities](src/sprite-utilities): contains a bunch of useful functions for creating Pixi sprites and making them easier to work with
- [Tile Utilities](src/tile-utilities): a collection of helpful methods and objects for using Tiled Editor with the Pixi renderering engine

Installation
------------
#### NPM
```
npm install pexi --save
```
#### CDN
```
<script src="https://unpkg.com/pexi/dist/pexi.min.js"></script>
```

:warning: You still need to install Pixi.js somehow, this package does not include it. :warning:
