const path = require('path'); // eslint-disable-line
const baseConfig = require('./webpack.base.config'); // eslint-disable-line

module.exports = function factory (projectRoot, libraryName) {
  return Object.assign({}, baseConfig, {
    entry: path.resolve(projectRoot, 'index.js'),
    output: {
      path: path.resolve(projectRoot, 'dist'),
      filename: libraryName + '.js',
      library: libraryName
    }
  })
}
