const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('./webpack.base.config')
const {merge} = require('lodash')

delete webpackConfig.entry
delete webpackConfig.externals
delete webpackConfig.output.libraryTarget

process.env.BABEL_ENV = 'test'

merge(webpackConfig, {
  devtool: 'inline-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      pexi: path.resolve(__dirname, '../src/pexi')
    })
  ]
})

module.exports = function (config) {
  config.set({
    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../test/unit',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'sinon-chai'],

    // list of files / patterns to load in the browser
    files: [
      './index.js'
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      './index.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: true,
      stats: 'none'
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['html', 'mocha', 'coverage'],

    htmlReporter: {
      outputFile: path.resolve(__dirname, '../test/reports/unit/index.html'),
      pageTitle: 'Pexi Unit Test Report',
      groupSuites: true
    },

    coverageReporter: {
      dir: path.resolve(__dirname, '../test/reports/coverage'),
      reporters: [
        { type: 'lcov', subdir: '.' },
        { type: 'text-summary' }
      ]
    },

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_WARN,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],

    customLaunchers: {
      'Chrome_no_security': {
        base: 'Chrome',
        flags: ['--disable-web-security']
      }
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
